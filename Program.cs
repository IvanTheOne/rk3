﻿using System;
using System.IO;


namespace RK3
{
    class Program
    {
        //Инит метод при старте программы
        public static void Main(string[] args)
        {
            //Переменная даты, нужна для сравнения
            DateTime time = DateTime.Now;
            //Переменная пути откуда мы дальше будем выполнять скрипты, является нашим хранилищем.
            //Увы, в этой версии, папку необходимо наполнять контентом, размер которых превышает 100 КБайт 
            string path = @"D:\Откуда";
            //Предоставляет методы экземпляра класса для создания, перемещения и перечисления в каталогах
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            //Если директория не создана - создаем, нет - пишем "Папка уже создана"
            if (!dirInfo.Exists)
            {
                Directory.CreateDirectory(path);
            }
            else
            {
                Console.WriteLine("Папка уже создана");
            }

            MoveFile();
            MoveToArchive();
            FileHandler();
        }


        //Разработать и протестировать макрос, который создает папку «Склад» и перемещает в нее все файлы из заданной папки, размер которых превышает 100 КБайт, если они есть.
        public static void MoveFile()
        {
            //Задаем переменные путей для папок
            string directoryPath = @"D:\Откуда";
            string newDirectoryPath = @"D:\Склад";
            //Если папка "Откуда" существует то в цикле ищем все файлы размером более 100000 байт
            if (Directory.Exists(directoryPath))
            {
                foreach (var file in new DirectoryInfo(directoryPath).GetFiles())
                {
                    if (file.Length > 100000)
                    {
                        //Перемещаем файл с помощью класса DirectoryInfo
                        file.MoveTo($@"{newDirectoryPath}\{file.Name}");
                        Console.WriteLine("Перемещаю файл " + file.Name + " в папку " + newDirectoryPath);
                    }
                else
                    {   // Если файлов размером более 100000 байт нет, пишем об их отсутствии
                        Console.WriteLine("В заданной папке нет файлов > 100 Кбайт");
                    }
                }
            }
        }


        //Разработать и протестировать макрос, который создает папку «Архив» и перемещает в нее все файлы из заданной папки, созданные до 01.10.2017, если они есть
        public static void MoveToArchive()
        {
            //Задаем переменные путей для папок
            string archivePath = @"D:\Архив";
            string directory = @"D:\Откуда";
            //Переменная требуемой даты, используем классы управления папками и датой
            DateTime d1 = new DateTime(2017, 01, 10, 6, 20, 40);
            DirectoryInfo dirInfo = new DirectoryInfo(directory);
            //Если папки нет, создаем :) иначе выводим "Папка уже создана"
            if (!dirInfo.Exists)
            {
                Directory.CreateDirectory(directory);
            }
            else
            {
                Console.WriteLine("Папка уже создана");
            }
            // Работаем с директорией в цикле, проверяем дату создания файла и сравниваем с объявленной переменной даты
            if (Directory.Exists(directory))
                {
                foreach (var file in new DirectoryInfo(directory).GetFiles())
                {
                    if (file.CreationTime <= d1)
                    {   // Если Дата создания файла меньше или равна 01.10.2017, то перемещаем файл в архив
                        file.MoveTo($@"{archivePath}\{file.Name}");
                    }
                    else
                    {  // Если таких файлов нет - пишем уведомление в консоль
                        Console.WriteLine("В заданной папке нет файлов созданных до 01.10.2017");
                    }
                }
            }
        }


        //Разработать и протестировать макрос, который создает файл «Список.txt» и записывает в него имена всех файлов заданной папки
        public static void FileHandler()
        {
            //Задаем переменные путей для папок
            string directory = @"D:\Откуда";
            string fileName = "Список.txt";
            // Соединяем в одну переменную название файла и путь к нему
            string pathString = System.IO.Path.Combine(directory, fileName);
            //Проверяем что
            Console.WriteLine("Путь до файла куда пишем названия других файлов: {0}\n ", pathString);
            //Используем класс StreamWriter для итерации в цикле по файлам, получения их имен и записи в файл
            using (StreamWriter fstream = new StreamWriter(pathString, true, System.Text.Encoding.Default))
            {
                foreach (var file in new DirectoryInfo(directory).GetFiles())
                {
                    fstream.WriteLine(file.Name);
                }
            }
        }
    }
}
